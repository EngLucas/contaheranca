class TestaContas {
	public static void main (String[] args){

	// Criar objetos 
	Conta c = new Conta();
	ContaCorrente cc = new ContaCorrente();
	ContaPoupanca cp = new ContaPoupanca();

	// chamando os Métodos
	c.deposita(1000);
	cc.deposita(1000);
	cp.deposita(1000);
	c.atualiza(0.01);
	cc.atualiza(0.01);
	cp.atualiza(0.01);

	// printa os valores das contas

	System.out.println(c.getSaldo());
	System.out.println(cc.getSaldo());
	System.out.println(cp.getSaldo());
	}
}


